from os import listdir as ls
from os.path import isdir, join


lv = "║"
lh = "═"
t = "╠"
e = "╚"


def tree(path="./", tab=2, level=0):
    files = ls(path)
    print("/".join(path.split("/")[1:]) if level else path)
    for index, file in enumerate(files):
        print((lv + " " * tab) * level + \
              (t if index != len(files)-1 else e) + \
              lh * (tab - 1) + " ", end="")
        if not isdir(join(path, file)):
            print(file)
        else:
            tree(join(path, file), level=level+1)


if __name__ == "__main__":
    from sys import argv

    if len(argv) > 1:
        for index, path in enumerate(argv[1:]):
            tree(path)
            if index != len(argv)-2:
                print()
    else:
        tree()
